<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package phpmstart
 */

?>

<?php
if ( is_customize_preview() ) {
	echo '<div id="phpmstart-footer-control" style="margin-top:-30px;position:absolute;"></div>';
}
?>
<footer class="footer" id="colophon">

</footer>
<?php wp_footer(); ?>
</body>
</html>
