<?php

/**
 *
 * This theme uses PSR-4 and OOP logic instead of procedural coding
 * Every function, hook and action is properly divided and organized inside related folders and files
 * Use the file `inc/Custom/Custom.php` to write your custom functions
 *
 * @package phpmstart
 */

if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.0' );
}
if ( ! defined('_DOMAIN_NAME')) {
	define('_DOMAIN_NAME', 'phpmstart');
}

if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname(__FILE__) . '/vendor/autoload.php';
}

if (class_exists('Phpmstart\\Init')) {
	Phpmstart\Init::register_services();
}