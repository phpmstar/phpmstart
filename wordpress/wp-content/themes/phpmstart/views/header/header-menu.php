<?php

use Phpmstart\Custom\CtaButton;

$ctaButton = new CtaButton();

$args = [
        'theme_location' => 'header',
        'container'       => 'ul',
        'menu_class'      => 'header__navbar--list',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

    ];
?>
<header class="header">
	<div class="container">
		<div class="header__content">
			<div class="header__logo">
				<?php the_custom_logo();?>
			</div>
            <nav class="header__navbar">
                <?php wp_nav_menu($args);?>
            </nav>
			<div class="header__button">
                <a href="<?php echo $ctaButton->show_url();?>" class="header__cta cta-button btn"><?php echo $ctaButton->show_text();?></a>
			</div>
		</div>
	</div>
</header>
