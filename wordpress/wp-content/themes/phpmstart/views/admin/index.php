<?php
/**
 * Template part for displaying a custom Admin area
 *
 * @link https://developer.wordpress.org/reference/functions/add_menu_page/
 *
 * @package phpmstart
 */

?>

<div class="content">
	<div class="container">
		<h1><?php esc_html__('PHPMSTART Settings Page', _DOMAIN_NAME);?></h1>
		<?php settings_errors();?>
        <?php

        ?>
		<form method="post" action="options.php">
			<?php settings_fields('phpmstart_options_group');?>
			<?php do_settings_sections('phpmstart-header');?>
			<?php submit_button();?>
		</form>
	</div>
</div>
