<?php

namespace Phpmstart;

final class Init {
	/**
	 * @return array Full list of classes
	 */
	public static function get_services(): array {
		return [
			Setup\Enqueue::class,
			Setup\Setup::class,
			Setup\Menus::class,
			Api\Customizer::class,
			Api\Callbacks\SettingsCallback::class,
			Api\Widgets\TextWidget::class,
			Core\Tags::class,
            Custom\Admin::class,
		];
	}

	/**
	 * Loop through the classes, initialize them, and call the register() method if it exists
	 * @return void
	 */
	public static function register_services(): void {
		foreach ( self::get_services() as $class ) {
			$service = self::instantiate( $class );
			if ( method_exists( $service, 'register' ) ) {
				$service->register();
			}
		}
	}

	/**
	 * Initialize the class
	 * @param class $class                  class from the services array
	 * @return class instance mixed         new instance of the class
	 */
	private static function instantiate( $class ) {
		return new $class();
	}
}