<?php

namespace Phpmstart\Api\Widgets;

use WP_Widget;

class TextWidget extends WP_Widget {
	public $widget_ID;
	public $widget_name;

	public $widget_options = [];

	public $control_options = [];

	/**
	 * register default hooks and actions for WordPress
	 * @return
	 */

	public function register(): void {
		parent::__construct( $this->widget_ID, $this->widget_name, $this->widget_options, $this->control_options );

		add_action( 'widgets_init', [ $this, 'widgetsInit' ] );
	}

	function __construct() {
		$this->widget_ID      = 'widget_phpmstart';
		$this->widget_name    = 'Phpmstart Custom Text';
		$this->widget_options = [
			'classname'                   => $this->widget_ID,
			'description'                 => $this->widget_name . 'Widget',
			'customize_selective_refresh' => true,

		];

		$this->control_options = [
			'width'  => 400,
			'height' => 350
		];
	}

	/**
	 * @return void
	 */
	public function widgetsInit(): void {
		register_widget( $this );
	}

	/**
	 * @param $args
	 * @param $instance
	 *
	 * @return void
	 */
	public function widget( $args, $instance ): void {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['filter_title'];
		}
		echo $args['after_widget'];
	}

	/**
	 * @param $instance
	 *
	 * @return void
	 */
	public function form($instance): void {
		$title = ! empty ($instance['title']) ? $instance['title'] : esc_html__('Custom Text', _DOMAIN_NAME);
		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title'));?>"><?php esc_attr_e('Title', _DOMAIN_NAME);?></label>
			<input name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
		</p>
<?php
	}

	/**
	 * @param $new_instance
	 * @param $old_instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ): array {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field($new_instance['title']);

		return $instance;
	}
}