<?php
/**
 * Theme Customizer - Footer
 *
 * @package phpmstart
 */

namespace  Phpmstart\Api\Customizer;

use WP_Customize_Control;
use WP_Customize_Color_Control;

use Phpmstart\Api\Customizer;

class Footer
{
	/**
	 * @param $wp_customize
	 *
	 * @return void
	 */
	public function register($wp_customize): void {
		$wp_customize->add_section('phpmstart_footer_section', [
			'title' => __('Footer', _DOMAIN_NAME),
			'description' => __('Customize the Footer', _DOMAIN_NAME),
			'priority' => 162
		]);
		$wp_customize->add_setting( 'phpmstart_footer_background_color' , [
			'default' => '#ffffff',
			'transport' => 'postMessage',
		]);

		$wp_customize->add_setting( 'phpmstart_footer_copy_text' ,[
			'default' => 'Proudly powered by Phpmstart',
			'transport' => 'postMessage',
		]);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'phpmstart_footer_background_color',[
			'label' => __( 'Background Color', _DOMAIN_NAME ),
			'section' => 'phpmstart_footer_section',
			'settings' => 'phpmstart_footer_background_color',
		]));

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'phpmstart_footer_copy_text', [
			'label' => __( 'Copyright Text', _DOMAIN_NAME ),
			'section' => 'phpmstart_footer_section',
			'settings' => 'phpmstart_footer_copy_text',
		] ) );

		if ( isset( $wp_customize->selective_refresh ) ) {
			$wp_customize->selective_refresh->add_partial( 'phpmstart_footer_background_color', [
				'selector' => '#phpmstart-footer-control',
				'render_callback' => [ $this, 'outputCss' ],
				'fallback_refresh' => true
			]);

			$wp_customize->selective_refresh->add_partial( 'phpmstart_footer_copy_text', [
				'selector' => '#phpmstart-footer-copy-control',
				'render_callback' => [ $this, 'outputText' ],
				'fallback_refresh' => true
			] );
		}
	}

	/**
	 * Generate inline CSS for customizer async reload
	 */
	public function outputCss(): void {
		echo '<style type="text/css">';
		echo Customizer::css( '.site-footer', 'background-color', 'phpmstart_footer_background_color' );
		echo '</style>';
	}

	/**
	 * Generate inline text for customizer async reload
	 */
	public function outputText(): void {
		echo Customizer::text( 'phpmstart_footer_copy_text' );
	}
}