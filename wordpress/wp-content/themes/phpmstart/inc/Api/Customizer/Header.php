<?php
/**
 * Theme Customizer - Header
 *
 * @package phpmstart
 */

namespace Phpmstart\Api\Customizer;

use WP_Customize_Color_Control;
use Phpmstart\Api\Customizer;

/**
 * Customizer class
 */

class Header
{
	/**
	 * @param $wp_customize
	 *
	 * @return void
	 */
	public function register($wp_customize): void {
		$wp_customize->add_section('phpmstart_header_section', [
			'title' =>  __('Header', _DOMAIN_NAME),
			'description' => __('Customize the Header', _DOMAIN_NAME),
			'priority'  =>  35
		]);

		$wp_customize->add_setting('phpmstart_header_background_color',[
			'default'   =>  '#ffffff',
			'transport' => 'postMessage',
		]);
		$wp_customize->add_setting('phpmstart_header_text_color', [
			'default' => '#333333',
			'transport' => 'postMessage',
		]);
		$wp_customize->add_setting('phpmstart_header_link_color', [
			'default' => '#004888',
			'transport' => 'postMessage'
		]);
		$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'phpmstart_header_background_color',[
			'label' => __('Header Background Color', _DOMAIN_NAME),
			'section' => 'phpmstart_header_section',
			'settings' => 'phpmstart_header_text_color',
		]));
		$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'phpmstart_header_text_color',[
			'label' => __('Header Text Color', _DOMAIN_NAME),
			'section' => 'phpmstart_header_section',
			'settings' => 'phpmstart_header_text_color',
		]));
		$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'phpmstart_header_link_color',[
			'label' => __('Header Link Color', _DOMAIN_NAME),
			'section' => 'phpmstart_header_section',
			'settings' => 'phpmstart_header_link_color',
		]));

		$wp_customize->get_setting('blogname')->transport = 'postMessage';
		$wp_customize->get_setting('blogdescription')->transport = 'postMessage';

		if ( isset($wp_customize->selective_refresh)) {
			$wp_customize->selective_refresh->add_partial('blogname', [
				'selector' => '.site-title a',
				'render_callback' => function(){
						bloginfo('name');
				}
			]);
			$wp_customize->selective_refresh->add_partial('blogdescription', [
				'selector' => '.site-description',
				'render_callback' => function(){
						bloginfo('description');
				}
			]);
			$wp_customize->selective_refresh->add_partial('phpmstart_header_background_color', [
				'selector' => '#phpsmtart-header-control',
				'render_callback' => [$this,'outputCss'],
				'fallback_refresh' => true
			]);
			$wp_customize->selective_refresh->add_partial('phpmstart_header_text_color', [
				'selector' => '#phpsmtart-header-control',
				'render_callback' => [$this,'outputCss'],
				'fallback_refresh' => true
			]);
			$wp_customize->selective_refresh->add_partial('phpmstart_header_link_color', [
				'selector' => '#phpsmtart-header-control',
				'render_callback' => [$this,'outputCss'],
				'fallback_refresh' => true
			]);
		}
	}

	/**
	 * @return void
	 */
	public function outputCss(): void {
		echo '<style type="text/css">';
		echo Customizer::css( '.site-header', 'background-color', 'phpmstart_header_background_color' );
		echo Customizer::css( '.site-header', 'color', 'phpmstart_header_text_color' );
		echo Customizer::css( '.site-header a', 'color', 'phpmstart_header_link_color' );
		echo '</style>';
	}
}