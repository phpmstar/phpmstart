<?php
/**
 * Theme Customizer - Sidebar
 *
 * @package phpmstart
 */

namespace Phpmstart\Api\Customizer;

use WP_Customize_Color_Control;
use Phpmstart\Api\Customizer;

/**
 * Customizer class
 */

class Sidebar
{
	/**
	 * @param $wp_customize
	 *
	 * @return void
	 */
	public function register($wp_customize): void {
		$wp_customize->add_section('phpmstart_sidebar_section',[
			'title' => __('Sidebar', _DOMAIN_NAME),
			'description' => __('Customize the Sidebar', _DOMAIN_NAME),
			'priority' => 161
		]);

		$wp_customize->add_setting('phpmstart_sidebar_background_color', [
			'default' => '#ffffff',
			'transport' => 'postMessage',
		]);
		$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'phpmstart_sidebar_background_color', [
			'selector' => '#phpmstart-sidebar-control',
			'render_callback' => [$this, 'output'],
			'fallback_refresh' => true
		]));

		if (isset($wp_customize->selective_refresh)) {
			$wp_customize->selective_refresh->add_partial('phpmstart_sidebar_background_color', [
				'selector' => '#phpmstart-sidebar-control',
				'render_callback' => [$this, 'output'],
				'fallback_refresh' => true
			]);
		}
	}
	/**
	 * Generate inline CSS for customizer async reload
	 */
	public function output(): void {
		echo '<style type="text/css">';
		echo Customizer::css( '#sidebar', 'background-color', 'phpmstart_sidebar_background_color' );
		echo '</style>';
	}
}