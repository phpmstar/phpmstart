<?php

namespace  Phpmstart\Api;

use Phpmstart\Api\Customizer\Sidebar;
use Phpmstart\Api\Customizer\Header;
use Phpmstart\Api\Customizer\Footer;

class Customizer
{
	/**
	 * @return void
	 */
	public function register(): void {
		add_action( 'wp_head', array($this , 'output') );
		add_action('customize_register', array($this, 'setup'));
	}

	/**
	 * @return array()
	 */
	public function get_classes(): array {
		return [
				Sidebar::class,
				Footer::class,
				Header::class
		];
	}

	/**
	 * @param $wp_customizer
	 *
	 * @return void
	 */
	public function setup($wp_customizer): void {
		foreach ($this->get_classes() as $class) {
			$service = new $class;
			if (method_exists($class,'register')){
				$service->register($wp_customizer);
			}
		}
	}

	/**
	 * @return void
	 */
	public function output(): void {
		echo '<!--Customizer CSS--> <style type="text/css">';
		echo self::css( '#sidebar', 'background-color', 'phpmstart_sidebar_background_color' );
		echo self::css( '.site-footer', 'background-color', 'phpmstart_footer_background_color' );
		echo self::css( '.site-header', 'background-color', 'phpmstart_header_background_color' );
		echo self::css( '.site-header', 'color', 'phpmstart_header_text_color' );
		echo self::css( '.site-header a', 'color', 'phpmstart_header_link_color' );
		echo '</style><!--/Customizer CSS-->';
	}

	public static function css( $selector, $property, $theme_mod )
	{
		$theme_mod = get_theme_mod($theme_mod);

		if ( ! empty( $theme_mod ) ) {
			return sprintf('%s { %s:%s; }', $selector, $property, $theme_mod );
		}
	}

	public static function text( $theme_mod )
	{
		$theme_mod = get_theme_mod($theme_mod);

		if ( ! empty( $theme_mod ) ) {
			return $theme_mod;
		}
	}

}