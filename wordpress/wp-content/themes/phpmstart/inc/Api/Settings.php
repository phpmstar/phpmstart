<?php
/**
 * Settings API
 *
 * @package phpmstart
 */

namespace Phpmstart\Api;

class Settings
{
    /**
     * @var array
     */
    public array $settings = [];

    /**
     * @var array
     */
    public array $sections = [];

    /**
     * @var array
     */
    public array $fields = [];

    /**
     * @var array
     */
    public array $admin_pages = [];

    /**
     * @var array
     */
    public array $admin_subpages = [];

    public function __construct()
    {}

    /**
     * @return void
     */
    public function register(): void
    {
        if ( ! empty($this->admin_pages) || ! empty($this->admin_subpages)) {
            add_action('admin_menu', [$this, 'add_admin_menu']);
        }

        if ( ! empty($this->settings)) {
            add_action('admin_init', [$this, 'register_custom_settings']);
        }
    }

    public function addPages($pages)
    {
        $this->admin_pages = $pages;

        return $this;
    }

    public function withSubPage($title = null)
    {
        if (empty($this->admin_pages)) {
            return $this;
        }

        $adminPage = $this->admin_pages[0];

        $subpage = [
            [
                'parent_slug' => $adminPage['menu_slug'],
                'page_title'  => $adminPage['page_title'],
                'menu_title'  => ($title) ? $title : $adminPage['menu_title'],
                'capability'  => $adminPage['capability'],
                'menu_slug'   => $adminPage['menu_slug'],
                'callback'    => $adminPage['callback']
            ]
        ];

        $this->admin_subpages = $subpage;

        return $this;
    }

    public function addSubPages($pages)
    {
        $this->admin_subpages = (count($this->admin_subpages) == 0) ? $pages : array_merge($this->admin_subpages, $pages);

        return $this;
    }

    public function add_admin_menu()
    {
        foreach ($this->admin_pages as $page) {
            add_menu_page(
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['menu_slug'],
                $page['callback'],
                $page['position']
            );
        }

        foreach ($this->admin_subpages as $page) {
            add_submenu_page(
                $page['parent_slug'],
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['menu_slug'],
                $page['callback']
            );
        }
    }

    public function add_settings($args)
    {
        $this->settings = $args;

        return $this;
    }

    public function add_sections($args)
    {
        $this->sections = $args;

        return $this;
    }

    public function add_fields($args)
    {
        $this->fields = $args;

        return $this;
    }

    public function register_custom_settings()
    {
        foreach ($this->settings as $setting) {
            register_setting(
                $setting["option_group"],
                $setting["option_name"],
                ($setting["callback"] ?? '')
            );
        }

        foreach ($this->sections as $section) {
            add_settings_section(
                $section["id"],
                $section["title"],
                ($section["callback"] ?? ''),
                $section["page"]
            );
        }

        foreach ($this->fields as $field) {
            add_settings_field(
                $field["id"],
                $field["title"],
                ($field["callback"] ?? ''),
                $field["page"],
                $field["section"],
                ($field["args"] ?? '')
            );
        }
    }
}