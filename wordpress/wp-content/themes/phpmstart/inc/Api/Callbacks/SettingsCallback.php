<?php
/**
 * Callbacks for Settings API
 *
 * @package phpmstart
 */

namespace Phpmstart\Api\Callbacks;

class SettingsCallback
{

    public function admin_index()
    {
        return require_once(get_template_directory() . '/views/admin/index.php');
    }

    public function phpmstart_options_group($input)
    {
        return $input;
    }

    public function admin_header()
    {
        echo '<div class="content"><h1>' . __('Header setting page', _DOMAIN_NAME) . '</h1></div>';
    }
    public function phpmstart_admin_index()
    {
        echo __('Customize this Theme Settings section and add description and instructions', _DOMAIN_NAME);
    }

    public function button_url()
    {
        $button_url = esc_attr(get_option('button_url'));
        echo '<input id="button_url" type="text" class="regular-text" name="button_url" value="' . $button_url . '" placeholder="' . __(
                'Button url',
                _DOMAIN_NAME
            ) . '" />';
    }
    public function button_text()
    {
        $button_text = esc_attr(get_option('button_text'));
        echo '<input id="button_text" type="text" class="regular-text" name="button_text" value="' . $button_text . '" placeholder="' . __(
                'Button text',
                _DOMAIN_NAME
            ) . '" />';
    }
}