<?php

namespace Phpmstart\Custom;

use Phpmstart\Api\Settings;
use Phpmstart\Api\Callbacks\SettingsCallback;

class Admin
{
    public Settings $settings;

    public SettingsCallback $callback;

    public function __construct()
    {
        $this->settings = new Settings();
        $this->callback = new SettingsCallback();
    }

    public function register()
    {
        $this->pages()->settings()->sections()->fields()->register_settings();
    }

    private function register_settings()
    {
        $this->settings->register();
    }

    private function fields()
    {
        $args = [
            [
                'id'       => 'button_url',
                'title'    => __('Button Url', _DOMAIN_NAME),
                'callback' => [$this->callback, 'button_url'],
                'page'     => 'phpmstart-header',
                'section'  => 'admin_header',
                'args'     => [
                    'label_for' => 'button_url',
                    'class'     => ''
                ]
            ],
            [
                'id'       => 'button_text',
                'title'    => __('Button Text', _DOMAIN_NAME),
                'callback' => [$this->callback, 'button_text'],
                'page'     => 'phpmstart-header',
                'section'  => 'admin_header',
                'args'     => [
                    'label_for' => 'button_text',
                    'class'     => ''
                ]
            ]
        ];

        $this->settings->add_fields($args);

        return $this;
    }

    private function sections()
    {
        $args = [
            [
                'id'       => 'phpmstart_admin_index',
                'title'    => 'Theme Settings',
                'callback' => [$this->callback, 'phpmstart_admin_index'],
                'page'     => 'phpmstart'
            ],
            [
                'id'  => 'admin_header',
                'title' => 'Header Settings',
                'callback'  => [$this->callback, 'admin_header'],
                'page' => 'phpmstart-header'
            ]
        ];

        $this->settings->add_sections($args);

        return $this;
    }

    private function settings()
    {
        $args = [
            [
                'option_group' => 'phpmstart_options_group',
                'option_name'  => 'button_url',
                'callback'     => [$this->callback, 'phpmstart_options_group']
            ],
            [
                'option_group' => 'phpmstart_options_group',
                'option_name' =>  'button_text',
                'callback'    =>  [$this->callback, 'phpmstart_options_group']
            ]
        ];

        $this->settings->add_settings($args);

        return $this;
    }

    private function pages()
    {
        $admin_pages = [
            [
                'page_title' => __('Phpmstart Admin Page', 'phpmstart'),
                'menu_title' => 'Theme Settings',
                'capability' => 'manage_options',
                'menu_slug'  => 'phpmstart',
                'callback'   => [$this->callback, 'admin_index'],
                'position'   => 110
            ]
        ];

        $admin_subpages = [
            [
                'parent_slug' => 'phpmstart',
                'page_title'  => __('Header settings', _DOMAIN_NAME),
                'menu_title'  => __('Header', _DOMAIN_NAME),
                'capability'  => 'manage_options',
                'menu_slug'   => 'phpmstart-header',
                'callback'    => [$this->callback, 'admin_header']
            ]
        ];

        $this->settings->addPages($admin_pages)->withSubPage('Header Settings')->addSubPages($admin_subpages);

        return $this;
    }
}