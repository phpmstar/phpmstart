<?php

namespace Phpmstart\Custom;

class CtaButton
{
    /**
     * @var false|mixed|null
     */
    public $text;
    /**
     * @var false|mixed|null
     */
    public $url;

    public function __construct()
    {
        $this->url = get_option('button_url', 'localhost');
        $this->text = get_option('button_text', 'click me');
    }

	public function show_url()
	{
		return $this->url;
	}

	public function show_text()
	{
		return $this->text;
	}

}

