<?php

namespace Phpmstart\Setup;

class Menus
{
    /**
     * @return void
     */
    public function register(): void
    {
		add_action('after_setup_theme', array($this,'menus'));
	}

    /**
     * @return void
     */
    public function menus(): void
    {
		register_nav_menus([
			'header' => esc_html__('Header', _DOMAIN_NAME),
			'footer' => esc_html__('Footer', _DOMAIN_NAME)
		]);
	}
}