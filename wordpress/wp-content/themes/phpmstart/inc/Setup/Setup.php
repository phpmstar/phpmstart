<?php

namespace Phpmstart\Setup;

class Setup {

	/**
	 * @return void
	 */
	public function register(): void {
		add_action( 'after_setup_theme', array( $this, 'setup' ) );
		add_action( 'after_setup_theme', array( $this, 'content_width' ), 0 );
        add_action('after_setup_theme', array($this, 'translation'));
        add_filter('upload_mimes', array($this, 'add_upload_mimes_type'), 10, 1);
	}

    /**
     * @return void
     */
    public function setup(): void
    {

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		] );
		add_theme_support( 'custom-background', apply_filters( 'phpmstart_custom_background_args', [
			'default-color' => 'ffffff',
			'default-image' => '',
		] ) );
		add_theme_support( 'custom-logo', [
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		] );
	}

    /**
     * @return void
     */
    public function translation(): void
    {
        load_theme_textdomain( _DOMAIN_NAME, get_template_directory() . '/languages' );

    }

	/**
	 * @return void
	 */
	public function content_width(): void {
		$GLOBALS['content_width'] = apply_filters( 'content_width', 1440 );
	}

    public function add_upload_mimes_type($upload_mimes)
    {
        $upload_mimes['svg'] = 'image/svg+xml';
        $upload_mimes['svgz'] = 'image/svg+xml';

        return $upload_mimes;
    }
}