<?php

namespace Phpmstart\Setup;

class Enqueue {

	/**
	 * @return void
	 */
	public function register() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	public function enqueue_scripts(): void {
		wp_enqueue_script('customizer', get_template_directory_uri() . '/inc/JS/customizer.js', array(), _S_VERSION, true);
		wp_enqueue_script('navigation', get_template_directory_uri() . '/inc/JS/navigation.js', array(), _S_VERSION, true);
		wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css', array(), _S_VERSION, false);
		wp_enqueue_script('main', get_template_directory_uri() . '/assets/main.js', array(), _S_VERSION, true);
	}

}